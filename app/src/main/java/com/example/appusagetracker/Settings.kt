package com.example.appusagetracker

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class Settings : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        val home=findViewById<Button>(R.id.home)
        home.setOnClickListener{
            Home()
        }
        val statsPage:Button=findViewById(R.id.stats)
        statsPage.setOnClickListener{
            statsPage()
        }

        val SAdapter= SettingsAdapter(dbExtract(),this@Settings)

        val rView = findViewById<RecyclerView>(R.id.recyclerviewSettings)
        rView.layoutManager= LinearLayoutManager(this)
        rView.itemAnimator= DefaultItemAnimator()
        rView.adapter = SAdapter


    }
    private fun dbExtract(): ArrayList<SelectedApp> {
        var db: DBHandler? =null
        db = DBHandler(this@Settings)
        val dbcur=db.readableDatabase

        val rs=dbcur.rawQuery("SELECT * FROM AppData",null)
        val apps: ArrayList<SelectedApp> = ArrayList()
        while(rs.moveToNext())
        {
            val x=rs.getString(0)
            val isChecked=rs.getString(2)
            val app=packageManager.getApplicationInfo(x,0)
            val appName=app.loadLabel(packageManager).toString()
            val icon= app.loadIcon(packageManager)

            icon?.let { SelectedApp(appName, it,x,isChecked) }?.let { apps.add(it) }
                //Toast.makeText(this@Settings,isChecked, Toast.LENGTH_SHORT).show()
        }
        db.close()
        return apps
    }
    private fun Home()
    {
        val startAct = Intent(this@Settings, MainActivity::class.java)
        startActivity(startAct)
        this.finish()
    }
    private fun statsPage()
    {
        val startAct = Intent(this@Settings, Stats::class.java)
        startActivity(startAct)
        this.finish()
    }
}